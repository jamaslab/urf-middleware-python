#pragma once

#include <memory>
#include <pybind11/pybind11.h>

#include <urf/middleware/messages/Header.hpp>

namespace urf {
namespace middleware {
namespace messages {

class PyHeader {
 public:
    PyHeader();
    ~PyHeader() = default;

    bool isCompressed() const;
    bool requiresAck() const;
    uint32_t length() const;
    uint8_t writerId() const;
    uint64_t timestamp() const;
    uint8_t version() const;
    uint64_t delay() const;

    void isCompressed(bool compressed);
    void requiresAck(bool requiresAck);
    void length(uint32_t length);
    void writerId(uint8_t writerId);
    void timestamp(uint64_t timestamp);
    void delay(uint64_t delay);

    pybind11::bytes serialize() const;
    bool deserialize(const pybind11::bytes& buffer);

    std::vector<uint8_t> getBytes();
    void setBytes(const std::vector<uint8_t>& bytes);

 private:
    std::shared_ptr<Header> header_;
};

}  // namespace messages
}  // namespace middleware
}  // namespace urf
