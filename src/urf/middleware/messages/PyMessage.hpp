#pragma once

#include <iostream>

#include "urf/middleware/messages/Message.hpp"
#include "urf/middleware/messages/PyBody.hpp"
#include "urf/middleware/messages/PyHeader.hpp"

namespace urf {
namespace middleware {
namespace messages {

class PyMessage {
 public:
    PyMessage();
    ~PyMessage() = default;

    PyHeader& header();
    PyBody& body();

    pybind11::dict dict() const;

    Message getMessage();
    void setMessage(const Message& message);

 private:
    PyBody _body;
    PyHeader _header;
};



}  // namespace messages
}  // namespace middleware
}  // namespace urf
