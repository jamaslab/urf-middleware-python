#pragma once

#include <iostream>
#include <memory>

#include <pybind11/pybind11.h>

#include "urf/middleware/messages/PyHeader.hpp"

namespace urf {
namespace middleware {
namespace messages {

class PyBody {
 public:
    PyBody();
    ~PyBody() = default;

    pybind11::dict dict() const;

    pybind11::bytes serialize() const;
    bool deserialize(const pybind11::bytes& buffer);

    PyHeader getHeader() const;

    std::vector<uint8_t> getBytes() const;
    void setBytes(const std::vector<uint8_t>& bytes);

 private:
    pybind11::dict _dict;
};

} // namespace messages
} // namespace middleware
} // namespace urf
