#pragma once

#include <optional>
#include <vector>

#include <pybind11/stl.h>
#include <pybind11/functional.h>

#include <urf/middleware/sockets/Client.hpp>
#include <urf/middleware/messages/PyMessage.hpp>

namespace urf {
namespace middleware {
namespace sockets {

class PyClient {
 public:
    PyClient() = delete;
    explicit PyClient(const std::string& socket);
    PyClient(const PyClient&) = delete;
    PyClient(PyClient&&) = delete;
    ~PyClient() = default;

    bool open();
    bool close();
    bool isOpen();

    std::optional<messages::PyMessage> pull(std::optional<uint32_t> timeout);
    bool push(messages::PyMessage& message, bool requiresAck);
    bool onPush(const std::function<void(const messages::PyMessage&)>& callable);

    std::vector<messages::PyMessage> request(messages::PyMessage& message, std::optional<uint32_t> timeout);
    std::optional<messages::PyMessage> receiveRequest(std::optional<uint32_t> timeout);

    bool respond(messages::PyMessage& message);

    bool subscribe(const std::string& topic, std::optional<uint32_t> maxRate);
    bool unsubscribe(const std::string& topic);

    bool keepUpdateHistory(bool value);

    std::optional<messages::PyMessage> receiveUpdate(const std::string& topic,  std::optional<uint32_t> timeout);
    bool onUpdate(const std::string& topic, const std::function<void(const std::string&, const messages::PyMessage&)>& callable);

    bool publish(const std::string& topic, messages::PyMessage message, bool requiresAck);
    std::vector<std::string> availablePartnerTopics();

    bool addTopic(const std::string& topic);
    bool removeTopic(const std::string& topic);

 private:
    std::unique_ptr<Client> client_;
};

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
