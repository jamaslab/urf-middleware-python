#pragma once

#include <optional>
#include <vector>

#include <pybind11/stl.h>
#include <pybind11/functional.h>

#include <urf/middleware/messages/PyMessage.hpp>
#include <urf/middleware/sockets/Server.hpp>

namespace urf {
namespace middleware {
namespace sockets {

class PyServer {
 public:
    PyServer() = delete;
    PyServer(const std::string& serviceName, const std::vector<std::string>& sockets);
    PyServer(const PyServer&) = delete;
    PyServer(PyServer&&) = delete;
    ~PyServer() = default;

    bool open();
    bool close();
    bool isOpen();

    size_t connectedClientsCount();

    std::optional<messages::PyMessage> pull(std::optional<uint32_t> timeout);
    bool push(messages::PyMessage& message, bool requiresAck);
    bool onPush(const std::function<void(const messages::PyMessage&)>& callable);

    std::vector<messages::PyMessage> request(messages::PyMessage& message, std::optional<uint32_t> timeout);
    std::optional<messages::PyMessage> receiveRequest(std::optional<uint32_t> timeout);

    bool respond(messages::PyMessage& message);

    bool subscribe(const std::string& topic, std::optional<uint32_t> maxRate);
    bool unsubscribe(const std::string& topic);

    bool keepUpdateHistory(bool value);

    std::optional<messages::PyMessage> receiveUpdate(const std::string& topic,  std::optional<uint32_t> timeout);
    bool onUpdate(const std::string& topic, const std::function<void(const std::string&, const messages::PyMessage&)>& callable);

    bool publish(const std::string& topic, messages::PyMessage message, bool requiresAck);
    std::vector<std::string> availablePartnerTopics();

    bool addTopic(const std::string& topic);
    bool removeTopic(const std::string& topic);

 private:
    std::unique_ptr<Server> server_;
};

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
