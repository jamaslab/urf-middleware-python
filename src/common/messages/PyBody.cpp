#include "urf/middleware/messages/PyBody.hpp"

namespace py = pybind11;

namespace urf {
namespace middleware {
namespace messages {

PyBody::PyBody() :
    _dict() { }

pybind11::dict PyBody::dict() const {
    return _dict;
}

py::bytes PyBody::serialize() const {
    auto cbor2 = py::module_::import("cbor2");
    auto cborDump = cbor2.attr("dumps");
    auto retval = cborDump(_dict);

    return retval.cast<py::bytes>();
}

bool PyBody::deserialize(const py::bytes& buffer) {
    auto cbor2 = py::module_::import("cbor2");
    auto cborLoads = cbor2.attr("loads");
    auto retval = cborLoads(buffer);
    _dict = retval.cast<py::dict>();
    return true;
}

PyHeader PyBody::getHeader() const {
    PyHeader header;
    header.length(getBytes().size());
    return header;
}

std::vector<uint8_t> PyBody::getBytes() const {
    std::string_view view(serialize());
    return std::vector<uint8_t>(view.begin(), view.end());
}

void PyBody::setBytes(const std::vector<uint8_t>& bytes) {
    py::bytes b(reinterpret_cast<char*>(const_cast<uint8_t*>(bytes.data())), bytes.size());
    deserialize(b);
}


}  // namespace messages
}  // namespace middleware
}  // namespace urf
