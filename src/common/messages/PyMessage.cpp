#include <iostream>
#include <memory>

#include "urf/middleware/messages/PyMessage.hpp"

namespace urf {
namespace middleware {
namespace messages {

PyMessage::PyMessage()
    : _body()
    , _header() { }

PyHeader& PyMessage::header() {
    return _header;
}

PyBody& PyMessage::body() {
    return _body;
}

pybind11::dict PyMessage::dict() const {
    return _body.dict();
}

Message PyMessage::getMessage() {
    auto bytes = _body.getBytes();
    Body body_wrapped;
    body_wrapped.setBytes(std::move(bytes));
    Header header_wrapped(bytes.size(),
                          _header.writerId(),
                          _header.timestamp(),
                          _header.isCompressed(),
                          _header.requiresAck());
    return Message(body_wrapped, header_wrapped);
}

void PyMessage::setMessage(const Message& message) {
    _body.setBytes(message.body().serialize());
    auto header = message.header();
    _header.setBytes(header.serialize());
}

} // namespace messages
} // namespace middleware
} // namespace urf
