#include "urf/middleware/messages/PyHeader.hpp"

#include <string_view>

namespace urf {
namespace middleware {
namespace messages {

PyHeader::PyHeader() :
    header_(new Header()) { }

bool PyHeader::isCompressed() const {
    return header_->isCompressed();
}

bool PyHeader::requiresAck() const {
    return header_->requiresAck();
}

uint32_t PyHeader::length() const {
    return header_->length();
}

uint8_t PyHeader::writerId() const {
    return header_->writerId();
}

uint64_t PyHeader::timestamp() const {
    return header_->timestamp();
}

uint8_t PyHeader::version() const {
    return header_->version();
}

uint64_t PyHeader::delay() const {
    return header_->delay();
}

void PyHeader::isCompressed(bool compressed) {
    header_->isCompressed(compressed);
}

void PyHeader::requiresAck(bool requiresAck) {
    header_->isCompressed(requiresAck);
}

void PyHeader::length(uint32_t length) {
    header_->length(length);
}

void PyHeader::writerId(uint8_t writerId) {
    header_->writerId(writerId);
}

void PyHeader::timestamp(uint64_t timestamp) {
    header_->timestamp(timestamp);
}

void PyHeader::delay(uint64_t delay) {
    header_->delay(delay);
}

pybind11::bytes PyHeader::serialize() const {
    auto vec = header_->serialize();
    return pybind11::bytes(std::string(vec.begin(), vec.end()));
}

bool PyHeader::deserialize(const pybind11::bytes& buffer) {
    std::string_view view(buffer);
    return header_->deserialize(reinterpret_cast<const uint8_t*>(view.data()), view.size());
}

std::vector<uint8_t> PyHeader::getBytes() {
    return header_->serialize();
}

void PyHeader::setBytes(const std::vector<uint8_t>& bytes) {
    header_->deserialize(bytes);
}

}  // namespace messages
}  // namespace middleware
}  // namespace urf
