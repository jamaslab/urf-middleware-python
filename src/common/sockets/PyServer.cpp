#include "urf/middleware/sockets/PyServer.hpp"

#include <iostream>
#include <string>
#include <vector>

namespace urf {
namespace middleware {
namespace sockets {

namespace py = pybind11;
using messages::PyMessage;

PyServer::PyServer(const std::string& serviceName, const std::vector<std::string>& sockets)
    : server_(new Server(serviceName, sockets)) {
    server_->automaticallyDeserializeBody(false);
}

bool PyServer::open() {
    return server_->open();
}

bool PyServer::close() {
    return server_->close();
}

bool PyServer::isOpen() {
    return server_->isOpen();
}

size_t PyServer::connectedClientsCount() {
    return server_->connectedClientsCount();
}

std::optional<PyMessage> PyServer::pull(std::optional<uint32_t> timeout) {
    std::optional<messages::Message> message;
    {
        py::gil_scoped_release release;
        if (!timeout) {
            message = server_->pull();
        } else {
            message = server_->pull(std::chrono::milliseconds(timeout.value()));
        }
    }

    if (!message) {
        return std::nullopt;
    }

    PyMessage retval;
    retval.setMessage(message.value());
    return retval;
}

bool PyServer::push(messages::PyMessage& message, bool requiresAck) {
    return server_->push(message.getMessage(), requiresAck);
}

bool PyServer::onPush(const std::function<void(const messages::PyMessage&)>& callable) {
    return server_->onPush([callable](auto message) {
        py::gil_scoped_acquire acquire;
        PyMessage msg;
        msg.setMessage(message);
        callable(msg);
    });
}

std::vector<PyMessage> PyServer::request(PyMessage& message, std::optional<uint32_t> timeout) {
    std::vector<messages::Message> responses;
    auto request = message.getMessage();
    {
        py::gil_scoped_release release;
        if (!timeout) {
            responses = server_->request(request);
        } else {
            responses =
                server_->request(request, std::chrono::milliseconds(timeout.value()));
        }
    }


    std::vector<PyMessage> retval;
    for (auto& msg : responses) {
        PyMessage pyMsg;
        pyMsg.setMessage(msg);
        retval.push_back(pyMsg);
    }

    return retval;
}

std::optional<messages::PyMessage> PyServer::receiveRequest(std::optional<uint32_t> timeout) {
    std::optional<messages::Message> message;
    {
        py::gil_scoped_release release;

        if (!timeout) {
            message = server_->receiveRequest();
        } else {
            message = server_->receiveRequest(std::chrono::milliseconds(timeout.value()));
        }
    }

    if (!message) {
        return std::nullopt;
    }

    PyMessage retval;
    retval.setMessage(message.value());
    return retval;
}

bool PyServer::respond(messages::PyMessage& message) {
    return server_->respond(message.getMessage());
}

bool PyServer::subscribe(const std::string& topic, std::optional<uint32_t> maxRate) {
    if (maxRate) {
        return server_->subscribe(topic, std::chrono::milliseconds(maxRate.value()));
    } else {
        return server_->subscribe(topic);
    }
}
bool PyServer::unsubscribe(const std::string& topic) {
    return server_->unsubscribe(topic);
}

bool PyServer::keepUpdateHistory(bool value) {
    return server_->keepUpdateHistory(value);
}

std::optional<messages::PyMessage> PyServer::receiveUpdate(const std::string& topic,
                                                           std::optional<uint32_t> timeout) {
    std::optional<messages::Message> message;
    {
        py::gil_scoped_release release;

        if (!timeout) {
            message = server_->receiveUpdate(topic);
        } else {
            message = server_->receiveUpdate(topic, std::chrono::milliseconds(timeout.value()));
        }
    }

    if (!message) {
        return std::nullopt;
    }

    PyMessage retval;
    retval.setMessage(message.value());
    return retval;
}

bool PyServer::onUpdate(
    const std::string& topic,
    const std::function<void(const std::string&, const messages::PyMessage&)>& callable) {
    return server_->onUpdate(topic, [callable](auto t, auto message) {
        py::gil_scoped_acquire acquire;
        PyMessage msg;
        msg.setMessage(message);
        callable(t, msg);
    });
}

bool PyServer::publish(const std::string& topic, messages::PyMessage message, bool requiresAck) {
    return server_->publish(topic, message.getMessage(), requiresAck);
}

std::vector<std::string> PyServer::availablePartnerTopics() {
    return server_->availablePartnerTopics();
}

bool PyServer::addTopic(const std::string& topic) {
    return server_->addTopic(topic);
}

bool PyServer::removeTopic(const std::string& topic) {
    return server_->removeTopic(topic);
}

} // namespace sockets
} // namespace middleware
} // namespace urf
