#include "urf/middleware/sockets/PyClient.hpp"

#include <iostream>
#include <string>
#include <vector>

namespace py = pybind11;

namespace urf {
namespace middleware {
namespace sockets {

using messages::PyMessage;

PyClient::PyClient(const std::string& socket)
    : client_(new Client(socket)) {
    client_->automaticallyDeserializeBody(false);
}

bool PyClient::open() {
    return client_->open();
}

bool PyClient::close() {
    return client_->close();
}

bool PyClient::isOpen() {
    return client_->isOpen();
}

std::optional<PyMessage> PyClient::pull(std::optional<uint32_t> timeout) {
    std::optional<messages::Message> message;
    {
        py::gil_scoped_release release;
        if (!timeout) {
            message = client_->pull();
        } else {
            message = client_->pull(std::chrono::milliseconds(timeout.value()));
        }

        if (!message) {
            return std::nullopt;
        }
    }

    PyMessage retval;
    retval.setMessage(message.value());
    return retval;
}

bool PyClient::push(messages::PyMessage& message, bool requiresAck) {
    return client_->push(message.getMessage(), requiresAck);
}

bool PyClient::onPush(const std::function<void(const messages::PyMessage&)>& callable) {
    return client_->onPush([callable](auto message) {
        py::gil_scoped_acquire acquire;
        PyMessage msg;
        msg.setMessage(message);
        callable(msg);
    });
}

std::vector<PyMessage> PyClient::request(PyMessage& message, std::optional<uint32_t> timeout) {
    std::vector<messages::Message> responses;
    auto request = message.getMessage();
    {
        py::gil_scoped_release release;
        if (!timeout) {
            responses = client_->request(request);
        } else {
            responses =
                client_->request(request, std::chrono::milliseconds(timeout.value()));
        }
    }

    std::vector<PyMessage> retval;
    for (auto& msg : responses) {
        PyMessage pyMsg;
        pyMsg.setMessage(msg);
        retval.push_back(pyMsg);
    }
    return retval;
}

std::optional<messages::PyMessage> PyClient::receiveRequest(std::optional<uint32_t> timeout) {
    std::optional<messages::Message> message;
    {
        py::gil_scoped_release release;
        if (!timeout) {
            message = client_->receiveRequest();
        } else {
            message = client_->receiveRequest(std::chrono::milliseconds(timeout.value()));
        }
    }
    if (!message) {
        return std::nullopt;
    }

    PyMessage retval;
    retval.setMessage(message.value());
    return retval;
}

bool PyClient::respond(messages::PyMessage& message) {
    return client_->respond(message.getMessage());
}

bool PyClient::subscribe(const std::string& topic, std::optional<uint32_t> maxRate) {
    if (maxRate) {
        return client_->subscribe(topic, std::chrono::milliseconds(maxRate.value()));
    } else {
        return client_->subscribe(topic);
    }
}
bool PyClient::unsubscribe(const std::string& topic) {
    return client_->unsubscribe(topic);
}

bool PyClient::keepUpdateHistory(bool value) {
    return client_->keepUpdateHistory(value);
}

std::optional<messages::PyMessage> PyClient::receiveUpdate(const std::string& topic,
                                                           std::optional<uint32_t> timeout) {
    std::optional<messages::Message> message;
    {
        py::gil_scoped_release release;

        if (!timeout) {
            message = client_->receiveUpdate(topic);
        } else {
            message = client_->receiveUpdate(topic, std::chrono::milliseconds(timeout.value()));
        }
    }

    if (!message) {
        return std::nullopt;
    }

    PyMessage retval;
    retval.setMessage(message.value());
    return retval;
}

bool PyClient::onUpdate(
    const std::string& topic,
    const std::function<void(const std::string&, const messages::PyMessage&)>& callable) {
    return client_->onUpdate(topic, [callable](auto t, auto message) {
        py::gil_scoped_acquire acquire;
        PyMessage msg;
        msg.setMessage(message);
        callable(t, msg);
    });
}

bool PyClient::publish(const std::string& topic, messages::PyMessage message, bool requiresAck) {
    return client_->publish(topic, message.getMessage(), requiresAck);
}

std::vector<std::string> PyClient::availablePartnerTopics() {
    return client_->availablePartnerTopics();
}

bool PyClient::addTopic(const std::string& topic) {
    return client_->addTopic(topic);
}

bool PyClient::removeTopic(const std::string& topic) {
    return client_->removeTopic(topic);
}

} // namespace sockets
} // namespace middleware
} // namespace urf
