import glob, os, ctypes, sys
basedir = os.path.abspath(os.path.dirname(__file__))

if sys.platform.startswith('linux'):
    for file in glob.glob(basedir+"/*.so"):
        ctypes.CDLL(file)
elif  sys.platform.startswith('win32'):
    for file in glob.glob(basedir+"/*.dll"):
        ctypes.CDLL(file)

from .urf_middleware_py import *

__all__ = ["Server", "Client", "Message", "Body", "Header"]