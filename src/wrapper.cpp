#include <pybind11/pybind11.h>

#include "urf/middleware/messages/PyBody.hpp"
#include "urf/middleware/messages/PyHeader.hpp"
#include "urf/middleware/messages/PyMessage.hpp"
#include "urf/middleware/sockets/PyClient.hpp"
#include "urf/middleware/sockets/PyServer.hpp"

using namespace urf::middleware::messages;
using namespace urf::middleware::sockets;

namespace py = pybind11;

PYBIND11_MODULE(urf_middleware_py, o) {
    py::class_<PyHeader>(o, "Header")
        .def(py::init<>())
        .def("is_compressed",
             static_cast<bool (PyHeader::*)() const>(&PyHeader::isCompressed),
             "Returns if the body of the message is compressed")
        .def("requires_ack",
             static_cast<bool (PyHeader::*)() const>(&PyHeader::requiresAck),
             "Returns if the message requires acknowledgement from the transport protocol")
        .def("length",
             static_cast<uint32_t (PyHeader::*)() const>(&PyHeader::length),
             "Returns the length in bytes of the body")
        .def("writer_id",
             static_cast<uint8_t (PyHeader::*)() const>(&PyHeader::writerId),
             "Returns the writer identifier")
        .def("timestamp",
             static_cast<uint64_t (PyHeader::*)() const>(&PyHeader::timestamp),
             "Returns the timestamp of the message")
        .def("version",
             static_cast<uint8_t (PyHeader::*)() const>(&PyHeader::version),
             "Returns the version of the procol")
        .def("delay",
             static_cast<uint64_t (PyHeader::*)() const>(&PyHeader::delay),
             "Returns the message delay if timestamp is synchronized")
        .def("is_compressed",
             static_cast<void (PyHeader::*)(bool)>(&PyHeader::isCompressed),
             "Sets the is_compressed flag of the message")
        .def("requires_ack",
             static_cast<void (PyHeader::*)(bool)>(&PyHeader::requiresAck),
             "Sets the requires_ack flag of the message")
        .def("length",
             static_cast<void (PyHeader::*)(uint32_t)>(&PyHeader::length),
             "Sets the length in bytes of the body")
        .def("writer_id",
             static_cast<void (PyHeader::*)(uint8_t)>(&PyHeader::writerId),
             "Sets the writer identifier")
        .def("timestamp",
             static_cast<void (PyHeader::*)(uint64_t)>(&PyHeader::timestamp),
             "Sets the timestamp of the message")
        .def("delay",
             static_cast<void (PyHeader::*)(uint64_t)>(&PyHeader::delay),
             "Sets the message delay if timestamp is synchronized")
        .def("serialize", &PyHeader::serialize, "Serialize the header")
        .def("deserialize", &PyHeader::deserialize, "Deserialize the header");

    py::class_<PyBody>(o, "Body")
        .def(py::init<>())
        .def("serialize", &PyBody::serialize)
        .def("deserialize", &PyBody::deserialize)
        .def("get_header", &PyBody::getHeader)
        .def("__setitem__", [](const PyBody &b, const py::object& key, const py::object& item) {
            b.dict()[key] = item;
        })
        .def("__getitem__", [](const PyBody &b, const py::object& key) {
            return b.dict()[key];
        })
        .def("clear", [](const PyBody &b) {
            return b.dict().clear();
        })
        .def("__len__", [](const PyBody &b) {
            return py::len(b.dict());
        })
        .def("__repr__", [](const PyBody &b) {
            return py::str(b.dict());
        });

    py::class_<PyMessage>(o, "Message")
        .def(py::init<>())
        .def("header", &PyMessage::header)
        .def("body", &PyMessage::body)
        .def("__setitem__", [](const PyMessage &b, const py::object& key, const py::object& item) {
            b.dict()[key] = item;
        })
        .def("__getitem__", [](const PyMessage &b, const py::object& key) {
            return b.dict()[key];
        })
        .def("clear", [](const PyMessage &b) {
            return b.dict().clear();
        })
        .def("__len__", [](const PyMessage &b) {
            return py::len(b.dict());
        })
        .def("__repr__", [](const PyMessage &b) {
            return py::str(b.dict());
        });

    py::class_<PyClient>(o, "Client")
        .def(py::init<const std::string&>())
        .def("open", &PyClient::open)
        .def("close", &PyClient::close)
        .def("is_open", &PyClient::isOpen)
        .def("pull", &PyClient::pull, py::arg("timeout") = std::nullopt)
        .def("push", &PyClient::push, py::arg("message"), py::arg("requiresAck") = true)
        .def("on_push", &PyClient::onPush)
        .def("request", &PyClient::request, py::arg("message"), py::arg("timeout") = std::nullopt)
        .def("receive_request", &PyClient::receiveRequest, py::arg("timeout") = std::nullopt)
        .def("respond", &PyClient::respond)
        .def("receive_update", &PyClient::receiveUpdate, py::arg("topic"), py::arg("timeout") = std::nullopt)
        .def("on_update", &PyClient::onUpdate)
        .def("publish", &PyClient::publish, py::arg("topic"), py::arg("message"), py::arg("requires_ack") = true)
        .def("subscribe", &PyClient::subscribe, py::arg("topic"), py::arg("max_rate") = std::nullopt)
        .def("unsubscribe", &PyClient::unsubscribe)
        .def("keep_update_history", &PyClient::keepUpdateHistory)
        .def("add_topic", &PyClient::addTopic)
        .def("remove_topic", &PyClient::removeTopic)
        .def("available_partner_topics", &PyClient::availablePartnerTopics);
    
     py::class_<PyServer>(o, "Server")
        .def(py::init<const std::string&, const std::vector<std::string>&>())
        .def("open", &PyServer::open)
        .def("close", &PyServer::close)
        .def("is_open", &PyServer::isOpen)
        .def("connected_clients_count", &PyServer::connectedClientsCount)
        .def("pull", &PyServer::pull, py::arg("timeout") = std::nullopt)
        .def("push", &PyServer::push, py::arg("message"), py::arg("requiresAck") = true)
        .def("on_push", &PyServer::onPush)
        .def("request", &PyServer::request, py::arg("message"), py::arg("timeout") = std::nullopt)
        .def("receive_request", &PyServer::receiveRequest, py::arg("timeout") = std::nullopt)
        .def("respond", &PyServer::respond)
        .def("receive_update", &PyServer::receiveUpdate, py::arg("topic"), py::arg("timeout") = std::nullopt)
        .def("on_update", &PyServer::onUpdate)
        .def("publish", &PyServer::publish, py::arg("topic"), py::arg("message"), py::arg("requires_ack") = true)
        .def("subscribe", &PyServer::subscribe, py::arg("topic"), py::arg("max_rate") = std::nullopt)
        .def("unsubscribe", &PyServer::unsubscribe)
        .def("keep_update_history", &PyServer::keepUpdateHistory)
        .def("add_topic", &PyServer::addTopic)
        .def("remove_topic", &PyServer::removeTopic)
        .def("available_partner_topics", &PyServer::availablePartnerTopics);
}
