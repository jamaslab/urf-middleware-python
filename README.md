alias python=python3
sudo rm /usr/bin/python
sudo ln -s /usr/bin/python3 /usr/bin/python

python setup.py bdist_wheel

conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan
