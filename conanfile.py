import os
import glob
from conans import ConanFile, CMake, tools
from six import StringIO  # Python 2 and 3 compatible
import sys

class UrfMiddlewarePythonConan(ConanFile):
    name = "urf_middleware_python"
    version = "0.10.0"
    license = "MIT"
    author = "Giacomo Lunghi"
    url = "https://github.com/Jamaslab/urf_middleware_python"
    description = "Unified robotic framework middleware"
    settings = "os", "compiler", "build_type", "arch"
    requires = ("urf_middleware_cpp/0.10.0@uji-cirtesu-irslab+urobf+urf-middleware-cpp/stable", "pybind11/2.10.1")
    build_requires = ("cmake/3.25.0")
    generators = "cmake", "cmake_find_package"
    exports_sources = ["src/*", "CMakeLists.txt",
                       "LICENSE", "Version.txt", "README.md"]

    options = {
        "python_executable": [None, "ANY"],
    }
    default_options = {
        "python_executable": None
    }

    @property
    def cmake_build_folder(self):
       return f"build/{str(self.settings.os)}/{self.settings.build_type}"

    @property
    def python_executable(self):
        if not self.options.python_executable:
            return sys.executable
        else:
            return self.options.python_executable

    @property
    def python_version(self):
        versionBuf = StringIO()
        self.run(f"{self.python_executable} -c \"import sys; print('.'.join(map(str, sys.version_info[:3])))\"", output=versionBuf)
        
        return versionBuf.getvalue().strip('\n')

    def configure(self):
        self.options['urf_middleware_cpp'].shared = False

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = os.path.join(self.recipe_folder, self.cmake_build_folder, 'package/')
        cmake.definitions["PYBIND11_PYTHON_VERSION"] = self.python_version
        cmake.definitions["PYTHON_EXECUTABLE"] = self.python_executable

        cmake.configure(build_folder=self.cmake_build_folder)
        cmake.build()
        cmake.install()
        self.run(f"cd {os.path.join(self.recipe_folder, self.cmake_build_folder, 'package/')} && {self.python_executable} setup.py sdist bdist_wheel")
        self.run(f"mv {os.path.join(self.recipe_folder, self.cmake_build_folder, 'package/dist')}/*.whl {os.path.join(self.recipe_folder, 'package')}")

    def package(self):
        self.copy("*.whl", dst=os.path.join(self.recipe_folder, 'package'), src=os.path.join(self.recipe_folder, self.cmake_build_folder, 'package/dist'))
